from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import pycocotools.coco as coco
from pycocotools.cocoeval import COCOeval
import numpy as np
import json
import random
import os
import math

import torch.utils.data as data
from lore_tsr.models.modules.lineless_table_process import *

class TableDataset(data.Dataset):
    num_classes = 2
    table_size = 768
    default_resolution = [768, 768]

    mean = np.array([0.40789654, 0.44719302, 0.47026115],
                    dtype=np.float32).reshape(1, 1, 3)
    std  = np.array([0.28863828, 0.27408164, 0.27809835],
                    dtype=np.float32).reshape(1, 1, 3)
    
    def __init__ (self, data_dir, image_dir, split):
        super(TableDataset, self).__init__()
        self.split = split
        self.data_dir = data_dir
        self.image_dir = image_dir
        print(f"Load data from directory {self.image_dir}")
        _ann_name = {'train': 'train',
                     'val': 'val',
                     'test': 'test'}
        self.annot_path = os.path.join(self.data_dir,'{}.json').format(_ann_name[split])
        print(f"Load annotations from directory{self.annot_path}")

        self.max_objs = 300
        self.max_pairs = 900
        self.max_cors = 1200

        self.class_name = ['__background__', 'center', 'corner']
        self._valid_ids = [1, 2]
        self.cat_ids = {v: i for i, v in enumerate(self._valid_ids)}
            # 1: 0; 2: 1
        self.voc_color = [(v // 32 * 64 + 64, (v // 8)%4 * 64, v % 8 * 32) \
                          for v in range(1, self.num_classes + 1)]
        self._data_rng = np.random.RandomState(123)
        self._eig_val = np.array([0.2141788, 0.01817699, 0.00341571],
                                dtype=np.float32)
        self._eig_vec = np.array([
            [-0.58752847, -0.69563484, 0.41340352],
            [-0.5832747, 0.00994535, -0.81221408],
            [-0.56089297, 0.71832671, 0.41158938]
        ], dtype=np.float32)

        self.split = split
        print('==> initializing table {} data.'.format(split))
        self.coco = coco.COCO(self.annot_path)
        self.images = self.coco.getImgIds()
        self.num_samples = len(self.images)

        print('Loaded {} {} samples'.format(split, self.num_samples))

    def __len__ (self):
        return self.num_samples
    
    def _judge(self, box):
        # to create a box or quadrangle, we need at least 2 xs and 2 ys
        countx = len(list(set([box[0], box[2], box[4], box[6]])))
        county = len(list(set([box[1], box[3], box[5], box[7]])))

        if countx < 2 or county < 2: 
            return False
        
        return True
    
    def __getitem__ (self, idx):
        img_id = self.images[idx]
        file_name = self.coco.loadImgs(ids=[img_id])[0]['file_name']
        
        image_path = os.path.join(self.image_dir, file_name)
        ann_ids = self.coco.getAnnIds(imgIds=[img_id])
        anns = self.coco.loadAnns(ids=ann_ids)
        num_objs = min(len(anns), self.max_objs)
        num_cors = self.max_cors

        img = cv2.imread(image_path)
        img_size = img.shape

        height, width = img.shape[0], img.shape[1]

        c = np.array([0, 0], dtype=np.float32)
        input_h, input_w = 768, 768
        s = max(img.shape[0], img.shape[1]) * 1.0

        output_h = input_h // 4
        output_w = input_w // 4

        num_classes = self.num_classes
        
        """ROTATE IF ANY"""
        rot = 0

        trans_input = get_affine_transform_upper_left(c, s, rot, [input_w, input_h])
        trans_output = get_affine_transform_upper_left(c, s, rot, [output_w, output_h])
        trans_output_mk = get_affine_transform_upper_left(c, s, rot, [output_w, output_h])

        """CONSTRUCT OUTPUT"""
        
        hm = np.zeros((num_classes, output_h, output_w), dtype=np.float32)
        wh = np.zeros((self.max_objs, 8), dtype=np.float32)
        reg = np.zeros((self.max_objs*5, 2), dtype=np.float32)
        st = np.zeros((self.max_cors, 8), dtype=np.float32)
        hm_ctxy = np.zeros((self.max_objs, 2), dtype=np.float32)
        hm_ind = np.zeros((self.max_objs), dtype=np.int64)
        hm_mask = np.zeros((self.max_objs), dtype=np.uint8)
        mk_ind = np.zeros((self.max_cors), dtype=np.int64)
        mk_mask = np.zeros((self.max_cors), dtype=np.uint8)
        reg_ind = np.zeros((self.max_objs*5), dtype=np.int64)
        reg_mask = np.zeros((self.max_objs*5), dtype=np.uint8)
        ctr_cro_ind = np.zeros((self.max_objs*4), dtype=np.int64)
        log_ax = np.zeros((self.max_objs, 4), dtype=np.float32)
        cc_match = np.zeros((self.max_objs, 4), dtype=np.int64)
        h_pair_ind = np.zeros((self.max_pairs), dtype=np.int64)
        v_pair_ind = np.zeros((self.max_pairs), dtype=np.int64)
        draw_gaussian = draw_umich_gaussian

        gt_det = []
        corList = []
        point = []
        pair_mark = 0
        inp = cv2.warpAffine(img, trans_input, (input_w, input_h),flags=cv2.INTER_LINEAR)
          
        for k in range(num_objs):
            ann = anns[k]

            seg_mask = ann['segmentation'] #[[351.0, 73.0, 172.0, 70.0, 174.0, 127.0, 351.0, 129.0, 351.0, 73.0]]
            x1,y1 = seg_mask[0]
            x2,y2 = seg_mask[1]
            x3,y3 = seg_mask[2]
            x4,y4 = seg_mask[3]  

            CorNer = np.array([x1, y1, x2, y2, x3, y3, x4, y4])
            boxes = [[CorNer[0],CorNer[1]],[CorNer[2],CorNer[3]],\
                    [CorNer[4],CorNer[5]],[CorNer[6],CorNer[7]]]   
            cls_id = int(self.cat_ids[ann['category_id']])

            CorNer[0:2] = affine_transform(CorNer[0:2], trans_output_mk)
            CorNer[2:4] = affine_transform(CorNer[2:4], trans_output_mk)
            CorNer[4:6] = affine_transform(CorNer[4:6], trans_output_mk)
            CorNer[6:8] = affine_transform(CorNer[6:8], trans_output_mk)
            CorNer[[0,2,4,6]] = np.clip(CorNer[[0,2,4,6]], 0, output_w - 1)
            CorNer[[1,3,5,7]] = np.clip(CorNer[[1,3,5,7]], 0, output_h - 1)      

            if not self._judge(CorNer):
                continue

            maxx = max([CorNer[2*I] for I in range(0,4)])
            minx = min([CorNer[2*I] for I in range(0,4)])
            maxy = max([CorNer[2*I+1] for I in range(0,4)])
            miny = min([CorNer[2*I+1] for I in range(0,4)])
            h, w = maxy - miny, maxx - minx # bbox[3] - bbox[1], bbox[2] - bbox[0]

            # draw gaussian around the bbox
            if h > 0 and w > 0:
                radius = gaussian_radius((math.ceil(h), math.ceil(w)))
                radius = max(0, int(radius))
                center = np.array([maxx + minx/2.0, (maxy+miny)/2.0], dtype=np.float32)
                center_int = center.astype(np.int32)

                draw_gaussian(hm[cls_id], center_int, radius)

                for i in range(4):
                    Cor = np.array([CorNer[2*i],CorNer[2*i+1]], dtype=np.float32)
                    Cor_int = Cor.astype(np.int32)
                    Cor_key = str(Cor_int[0])+"_"+str(Cor_int[1])
                    if Cor_key not in corList:

                        corNum = len(corList)
                        
                        corList.append(Cor_key)
                        reg[self.max_objs+corNum] = np.array([abs(Cor[0]-Cor_int[0]),abs(Cor[1]-Cor_int[1])])
                        mk_ind[corNum] = Cor_int[1]*output_w + Cor_int[0]
                        cc_match[k][i] = mk_ind[corNum]
                        reg_ind[self.max_objs+corNum] = Cor_int[1]*output_w + Cor_int[0]
                        mk_mask[corNum] = 1
                        reg_mask[self.max_objs+corNum] = 1
                        draw_gaussian(hm[num_classes-1], Cor_int, 2)
                        st[corNum][i*2:(i+1)*2] = np.array([Cor[0]-center[0],Cor[1]-center[1]])
                        ctr_cro_ind[4*k+i] = corNum*4 + i

                    else:            
                        index_of_key = corList.index(Cor_key)
                        cc_match[k][i] = mk_ind[index_of_key]
                        st[index_of_key][i*2:(i+1)*2] = np.array([Cor[0]-center[0],Cor[1]-center[1]])
                        ctr_cro_ind[4*k+i] = index_of_key*4 + i
                
                wh[k] = center[0] - 1. * CorNer[0], center[1] - 1. * CorNer[1], \
                                center[0] - 1. * CorNer[2], center[1] - 1. * CorNer[3], \
                                center[0] - 1. * CorNer[4], center[1] - 1. * CorNer[5], \
                                center[0] - 1. * CorNer[6], center[1] - 1. * CorNer[7]
                hm_ind[k] = center_int[1] * output_w + center_int[0]
                hm_mask[k] = 1
                reg_ind[k] = center_int[1] * output_w + center_int[0]
                reg_mask[k] = 1
                reg[k] = center - center_int
                hm_ctxy[k] = center[0], center[1]

                log_ax[k] = ann['logic_axis'][0][0], ann['logic_axis'][0][1], ann['logic_axis'][0][2], ann['logic_axis'][0][3]

                gt_det.append([center[0] - 1. * CorNer[0], center[1] - 1. * CorNer[1],
                            center[0] - 1. * CorNer[2], center[1] - 1. * CorNer[3],
                            center[0] - 1. * CorNer[4], center[1] - 1. * CorNer[5], 
                            center[0] - 1. * CorNer[6], center[1] - 1. * CorNer[7], 1, cls_id])
                
        hm_mask_v = hm_mask.reshape(1, hm_mask.shape[0])    

        inp = (inp.astype(np.float32) / 255.)

        inp = (inp - self.mean) / self.std
        inp = inp.transpose(2, 0, 1)

        ret = {
            'input': inp, 
            'hm': hm, 
            'hm_ind': hm_ind, 
            'hm_mask': hm_mask, 
            'mk_ind': mk_ind, 
            'mk_mask': mk_mask, 
            'reg': reg,
            'reg_ind': reg_ind,
            'reg_mask': reg_mask, 
            'wh': wh,
            'st': st, 
            'ctr_cro_ind': ctr_cro_ind,
            'cc_match': cc_match, 
            'hm_ctxy': hm_ctxy, 
            'logic': log_ax, 
            # 'h_pair_ind': h_pair_ind, 
            # 'v_pair_ind': v_pair_ind
        }

        # if not self.split == 'train':
        #     meta = {'c': c, 's': s, 'rot':rot, 'gt_det': gt_det, 'img_id': img_id}
        #     ret['meta'] = meta

        return ret


