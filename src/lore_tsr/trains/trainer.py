from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import time
import torch
from progress.bar import Bar
from tqdm import tqdm

# from lore.lib.mopythdels.data_parallel import DataParallel
from lore_tsr.models.utils import AverageMeter

from lore_tsr.models.losses import FocalLoss
from lore_tsr.models.losses import RegL1Loss, AxisLoss
from lore_tsr.models.losses import PairLoss
from lore_tsr.models.modules.lineless_table_process import _sigmoid

torch.autograd.set_detect_anomaly(True)

class ModelWithLoss(torch.nn.Module):
    def __init__(self, detector, loss, hold_det = False, processor= None):
        super(ModelWithLoss, self).__init__()
        self.detector = detector
        self.processor = processor
        self.loss = loss
        self.phase = None

        if hold_det:
            for param in self.detector.named_parameters():
                param[1].requires_grad = False # freeze the detector
            
    def set_phase(self, phase):
        self.phase = phase

        if self.phase == "train": 
            self.processor.train()
            self.detector.train()
        else:
            self.processor.eval()
            self.detector.eval()

    def forward(self, epoch, batch):
        outputs = self.detector(batch['input'])

        if self.phase == 'train':
            logic_axis, stacked_axis = self.processor(outputs, batch)
        else:
            logic_axis, stacked_axis = self.processor(outputs, None)
        loss, loss_stats = self.loss(epoch, outputs, batch, logic_axis, stacked_axis)

        return outputs[-1], loss, loss_stats
    
class LoreTrainer(object):
    def __init__(self, detector, optimizer=None, processor=None, device="cuda"):
        self.device = torch.device(device)   

        self.optimizer = optimizer
        self.detector = detector.to(self.device)
        self.processor = processor.to(self.device)

        self.loss_stats, self.loss = self._get_losses()
        self.model_with_loss = ModelWithLoss(detector = self.detector, 
                                             loss= self.loss, 
                                             processor = self.processor)


    def set_device(self, device):
        # if len(gpus) > 1:
        #     self.model_with_loss = DataParallel(
        #         self.model_with_loss, device_ids=gpus,
        #         chunk_sizes=chunk_sizes).to(device)
        # else:
        #     self.model_with_loss = self.model_with_loss.to(device)
        self.model_with_loss = self.model_with_loss.to(device)
        
        for state in self.optimizer.state.values():
            for k, v in state.items():
                if isinstance(v, torch.Tensor):
                    state[k] = v.to(device = device, non_blocking=True)
    
    def run_epoch(self, phase, epoch, data_loader):
        model_with_loss = self.model_with_loss

        if phase == 'train':
            model_with_loss.set_phase(phase)
            model_with_loss.train()
        else:
            model_with_loss.set_phase(phase)
            torch.cuda.empty_cache()
            model_with_loss.eval()

        results = {}
        data_time, batch_time = AverageMeter(), AverageMeter()
        avg_loss_stats = {l: AverageMeter() for l in self.loss_stats}
        num_iters = len(data_loader)
        bar = Bar(max=num_iters)
        end = time.time()

        for iter_id, batch in tqdm(enumerate(data_loader)):
            if iter_id >= num_iters:
                break
            for k in batch:
                if k != 'meta':
                    batch[k] = batch[k].to(self.device)     

            data_time.update(time.time() - end)
            
            output, loss, loss_stats = model_with_loss(epoch, batch)
            if phase == 'train':
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
            batch_time.update(time.time() - end)
            end = time.time()

            Bar.suffix = '{phase}: [{0}][{1}][{2}]|Tot: {total:} |ETA: {eta:}'.format(
                epoch, iter_id, num_iters, phase=phase,
                total=bar.elapsed_td, eta=bar.eta_td
            )

            for l in avg_loss_stats:
                avg_loss_stats[l].update(
                    loss_stats[l].detach().cpu().numpy().mean(), batch['input'].size(0))
                if l == 'loss_mj' or l == 'loss_mn' or l == 'smo_loss':
                    Bar.suffix = Bar.suffix + '|{} {:.2f} '.format(1, avg_loss_stats[l].avg)
                else:
                    Bar.suffix = Bar.suffix + '|{} {:.4f} '.format(l, avg_loss_stats[l].avg)                      
                
            print_iter = 200
            if iter_id % print_iter == 0:
                print(' | {}'.format(Bar.suffix)) 
            else:
                bar.next()

            # self.save_result(output, batch, results)
        
        bar.finish()
        ret = {k: v.avg for k, v in avg_loss_stats.items()}
        ret['time'] = bar.elapsed_td.total_seconds() / 60

        return ret, results
    
    def val(self, epoch, data_loader):
        return self.run_epoch('val', epoch, data_loader)

    def train(self, epoch, data_loader):
        return self.run_epoch('train', epoch, data_loader)
    
    def _get_losses(self):
        loss_stats = ['loss', 'hm_l', 'wh_l', 'st_l', 'ax_l', 'sax_l']
        loss = LoreLoss()
        return loss_stats, loss

    
class LoreLoss(torch.nn.Module):
    def __init__(self):
        super(LoreLoss, self).__init__()
        self.crit = FocalLoss()
        self.crit_mk = FocalLoss()
        self.crit_reg = RegL1Loss()
        self.crit_wh = torch.nn.L1Loss()
        self.crit_st = self.crit_reg
        self.crit_ax = AxisLoss()
        self.pair_loss = PairLoss()
    
    def forward(self, epoch, outputs, batch, logi=None, slogi=None):
        '''
        hm, re, off, wh losses are original losse of CenterNet detector,
        and st loss is the loss for parsing-grouping in Cycle-CenterNet.
        '''
        '''hm_loss, st_loss, re_loss, off_loss, wh_loss, ax_loss, sax_loss, sm_loss'''
        
        output = outputs[-1]
        output['hm'] = _sigmoid(output['hm'])

        """LOSS FOR DETECTION MODULE"""

        # with pair_loss
        hm_loss = self.crit(output['hm'] , batch['hm'] ) 

        loss1, loss2 = \
        self.pair_loss(output['wh'],batch['hm_ind'],output['st'],batch['mk_ind'],batch['hm_mask'], \
        batch['mk_mask'],batch['ctr_cro_ind'],batch['wh'],batch['st'],batch['hm_ctxy'])
        
        wh_loss = loss1 
        st_loss = loss2 

        # reg_offset = True and off_weight > 0 (CycleCenterNet)
        off_loss = self.crit_reg(output['reg'], batch['reg_mask'], batch['reg_ind'], batch['reg'])    

        """LOSS FOR RECONSTRUCTION MODULE"""
        ax_loss = self.crit_ax(output['ax'], batch['hm_mask'], batch['hm_ind'], batch['logic'], logi)

        """STACKED AXIS LOSS"""
        sax_loss = self.crit_ax(output['ax'], batch['hm_mask'], batch['hm_ind'], batch['logic'], slogi)        

        """COMBINING LOSS"""

        hm_weight = 1.5
        wh_weight = 1
        off_weight = 1

        loss = hm_weight * hm_loss + wh_weight * wh_loss + \
               off_weight * off_loss + 2*ax_loss + \
               st_loss + 2 * sax_loss

        """CONSTRUCTING LOSS STATUS"""
        loss_stats = {
            'loss': loss,
            'hm_l': hm_loss,
            'wh_l': wh_loss,
            'ax_l': ax_loss,
            'st_l': st_loss,
            'sax_l': sax_loss
        }

        return loss, loss_stats