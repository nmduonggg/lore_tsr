import copy
import math
from os.path import join
from .transformer import Transformer
from .utils import _tranpose_and_gather_feat, _get_wh_feat, _get_4ps_feat, _normalized_ps

import numpy as np
import torch
from torch.autograd import Variable
import torch.nn.functional as F
from torch import nn

class Stacker(nn.Module):
    """
    The architecture of the stacking regressor, which takes the dense representation
    and logical locations of table cells to make more accurate perdiction of logical locations
    """

    def __init__ (self,
                  input_size,
                  hidden_size,
                  output_size,
                  layers, 
                  heads= 8,
                  dropout= 0.1):
        """
        Args:
            input_size: The dim of logical locations which is always 4
            hidden_size: The dim of hidden states which is 256 by default
            output_size: The dim of logical locations which is always 4
            layers: Number of layers of self-attenttion mechanism, which is 4 in this implementation
        """
        super(Stacker, self).__init__()
        self.logi_encoder = nn.Sequential(
            nn.Linear(input_size, hidden_size), 
            nn.ReLU(inplace=True),
            nn.Linear(hidden_size, hidden_size), 
            nn.ReLU(inplace= True))
        self.tsfm = Transformer(2 * hidden_size, hidden_size, output_size,
                                layers, heads, dropout)
        
    def forward(self, outputs, logi, mask= None, require_att= False):
        """
        Args:
            outputs: The dense representation of table cells, a tensor of [batch_size, number_of_objects, 4]
            mask: The mask of cells, a tensor of [batch_size, number_of_objects], not None only in training phase
            require_att: If true, model will generate the attention maps of table cells

        Returns:
            stacked_axis: The predicted logical location of cells, a tensor of [batch_size, number_of_objects, 4]
            att: The attention map of table cells
        """
        logi_embeddings = self.logi_encoder(logi)
        cat_embeddings = torch.cat((logi_embeddings, outputs), dim=2)

        if mask is None: # valid and test
            if require_att: 
                stacked_axis, att = self.tsfm(cat_embeddings)
            else:
                stacked_axis = self.tsfm(cat_embeddings)

        else: # training
            stacked_axis = self.tsfm(cat_embeddings, mask=mask)
        
        if require_att:
            return stacked_axis, att
        else:
            return stacked_axis

class LoreProcessor(nn.Module):
    """
    The logical location prediction head of LORE. It contains a base regresseor and
    a stacked regressor.
    They both consist of several self-attention blocks
    """

    def __init__(self, **kwargs):

        super(LoreProcessor, self).__init__()

        self.input_size = 256
        self.output_size = 4
        self.hidden_size = 256
        self.max_fmp_size = 256
        self.stacking_layers = 4
        self.tsfm_layers = 4
        self.num_heads = 8
        self.att_dropout = 0.1
        self.stacker = Stacker(self.output_size, self.hidden_size,
                               self.output_size, self.stacking_layers)
        self.tsfm_axis = Transformer(self.input_size, self.hidden_size,
                                     self.output_size, self.tsfm_layers, 
                                     self.num_heads, self.att_dropout)
        self.x_position_embeddings = nn.Embedding(self.max_fmp_size,
                                                  self.hidden_size)
        self.y_position_embeddings = nn.Embedding(self.max_fmp_size,
                                                  self.hidden_size)
        
    def forward(self, outputs, batch=None, cc_match=None, dets=None):
        """
        Args:
            outputs: The dense representation of table cell from the detection part
                of LORE, a tensor of [batch_size, number_of_objects, hidden_size]
            batch: The detection results of other source, such as external OCR systems
            dets: The detection results of each table cells, a tensor of
                [batch_size, number-of_objects, 8] => (x, y) for each corner

        Returns:
            logi_axis: The output logical location of base regressor, a tensor of
                [batch_size, number_of_objcets, 4]
            stacked_axis: The outut logical location of stacking regressor, a tensor
                of [batch_size, number_of_objects, 4]
        """

        '''Constructing Features'''
        if batch is None:
            # Inference mode, the four corner features are gathered 
            # during bounding boxes decoding for simplicity 
            vis_feat = outputs[-1]['ax']
            vis_feat = vis_feat.permute(0, 2, 3, 1).contiguous()

            if dets is None:
                feat = vis_feat
            else:
                left_pe = self.x_position_embeddings(dets[:, :, 0])
                upper_pe = self.y_position_embeddings(dets[:, :, 1])
                right_pe = self.x_position_embeddings(dets[:, :, 2])
                lower_pe = self.y_position_embeddings(dets[:, :, 5])
                feat = vis_feat + left_pe + upper_pe + right_pe + lower_pe
            
        else:
            # Training mode
            ind = batch['hm_ind']
            mask = batch['hm_mask'] # during training, the attention mask will be applied
            output = outputs[-1]
            pred = output['ax']
            ct_feat = _tranpose_and_gather_feat(pred, ind)

            # wiz_2dpe
            cr_feat = _get_4ps_feat(batch['cc_match'], output)
            cr_feat = cr_feat.sum(axis= 3)
            vis_feat = ct_feat + cr_feat

            ps = _get_wh_feat(ind, batch, 'gt')
            ps = _normalized_ps(ps, self.max_fmp_size)

            left_pe = self.x_position_embeddings(ps[:, :, 0])
            upper_pe = self.y_position_embeddings(ps[:, :, 1])
            right_pe = self.x_position_embeddings(ps[:, :, 2])
            lower_pe = self.y_position_embeddings(ps[:, :, 5])

            feat = vis_feat + left_pe + upper_pe + right_pe + lower_pe

            # wiz_4ps: use the feature of 4 corner points
            # cr_feat = _get_4ps_feat(batch['cc_match'])

        '''
            Put Features into TSFM:
        '''
        if batch is None:
            # Inference Mode
            # print(feat)
            logic_axis = self.tsfm_axis(feat)
            stacked_axis = self.stacker(feat, logic_axis)
        else: 
            # Training mode
            logic_axis = self.tsfm_axis(feat, mask=mask)
            stacked_axis = self.stacker(feat, logic_axis, mask=mask)

        return logic_axis, stacked_axis

    def load_processor(model,
                       model_path,
                       optimizer= None, 
                       resume= False,
                       lr = None, 
                       lr_step= None):
        checkpoint = torch.load(model_path, map_location= lambda storage, loc: storage)
        print(f"loaded {model_path}, epoch {checkpoint['epoch']}")
        state_dict = checkpoint['state_dict']
        model.load_state_dict(state_dict)
        return model
    
