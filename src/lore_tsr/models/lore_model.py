import copy
import math
from os.path import join
from typing import Any, Dict

import numpy as np
import torch
import torch.nn.functional as F
from torch import nn
import cv2

from lore_tsr.models.modules.lineless_table_process import *
from lore_tsr.models.detectors.lore_detector import LoreDetector
from lore_tsr.models.modules.lore_processor import LoreProcessor


class LoreModel(nn.Module):
    '''
    The model first locates table cells in the input image by key point segmentation.
    Then the logical locations are predicted along witht the spatial locations
    employing two cascading regressors.
    NOTE: This model is only used for inference, not training
    '''
    def __init__(self, model_dir: str,
                 detector_name: str,
                 processor_name: str,
                 device: str, **kwargs):
        '''
        initialize the LORE model from the  `model_dir` path

        Args:
            model_dir(str): the model path
        '''
        super(LoreModel, self).__init__()
        detector_path = join(model_dir, detector_name)
        processor_path = join(model_dir, processor_name)
        # detector_checkpoint = torch.load(detector_path, map_location= lambda storage, loc: storage)
        # processor_checkpoint = torch.load(processor_path, map_location= lambda storage, loc: storage)

        # init detect infer model
        self.detector = LoreDetector()
        # load_lore_model(self.detect_infer_model, model_checkpoint, 'model')
        load_model(self.detector, detector_path)

        # init process infer model
        self.processor = LoreProcessor()
        # load_lore_model(self.process_infer_model, processor_checkpoint, 'processor')
        load_model(self.processor, processor_path)
        self.device = torch.device(device)

    def preprocess(self, image, meta=None):
        """
        Preprocess image and get the metainfo of input image
        """
        K, MK = 3000, 5000
        height, width = image.shape[0:2]
        mean = np.array([0.408, 0.447, 0.470],
                        dtype=np.float32).reshape(1, 1, 3)
        std = np.array([0.289, 0.274, 0.278],
                       dtype=np.float32).reshape(1, 1, 3)

        # fix_res = True
        inp_height, inp_width = 768, 768
        c = np.array([0, 0], dtype=np.float32)
        s = max(height, width) * 1.0
        trans_input = get_affine_transform_upper_left(c, s, 0,
                                                      [inp_width, inp_height])

        resized_image = cv2.resize(image, (width, height))
        inp_image = cv2.warpAffine(
            resized_image,
            trans_input, (inp_width, inp_height),
            flags=cv2.INTER_LINEAR)
        inp_image = ((inp_image / 255. - mean) / std).astype(np.float32)

        images = inp_image.transpose(2, 0, 1).reshape(1, 3, inp_height,
                                                      inp_width)
        images = torch.from_numpy(images).to(self.device)
        meta = {
            'c': c,
            's': s,
            'input_height': inp_height,
            'input_width': inp_width,
            'out_height': inp_height // 4,
            'out_width': inp_width // 4
        }

        # result = {'img': images, 'meta': meta}

        return images, meta
    
    def filter(self, results, logi,ps):
        # this function selects boxes
        batch_size, feat_dim = logi.shape[0], logi.shape[2]
        num_valid = sum(results[1][:,8] >= 0.15) # vis_thresh = 0.15

        slct_logi = np.zeros((batch_size, num_valid, feat_dim), dtype = np.float32)
        slct_dets = np.zeros((batch_size, num_valid, 8), dtype = np.int32)
        for i in range(batch_size):
            for j in range(num_valid):
                slct_logi[i, j, :] = logi[i, j, :].cpu()
                slct_dets[i, j, :] = ps[i,j,:].cpu()

        return torch.Tensor(slct_logi).cuda(), torch.Tensor(slct_dets).cuda()
    
    def process_logi(self, logi):
        logi_floor = logi.floor()
        dev =logi - logi_floor
        logi = torch.where(dev > 0.5, logi_floor+1, logi_floor)

        return logi
    def _normalized_ps(self, ps, vocab_size):
        ps = torch.round(ps).to(torch.int64)
        ps = torch.where(ps < vocab_size, ps, (vocab_size-1) * torch.ones(ps.shape).to(torch.int64).cuda())
        ps = torch.where(ps >= 0, ps, torch.zeros(ps.shape).to(torch.int64).cuda())
        return ps
    
    def ps_convert_minmax(self, results):
        detection = {}
        for j in range(1, 3): # 2 classes only
            detection[j] = []
        for j in range(1, 3):
            for bbox in results[j]:
                minx = min(bbox[0],bbox[2],bbox[4],bbox[6])
                miny = min(bbox[1],bbox[3],bbox[5],bbox[7])
                maxx = max(bbox[0],bbox[2],bbox[4],bbox[6])
                maxy = max(bbox[1],bbox[3],bbox[5],bbox[7])
                detection[j].append([minx, miny, maxx, maxy, bbox[-1]])
        for j in range(1, 3):
            detection[j] = np.array(detection[j])
            return detection
        
    def process(self, images, origin, return_time=True):
        K, MK = 3000, 5000
        with torch.no_grad():
            outputs = self.detector(images)
            output = outputs[-1]

            hm = output['hm'].sigmoid_()
            wh = output['wh']
            reg = output['reg']
            st = output['st']
            ax = output['ax']
            cr = output['cr']

            torch.cuda.synchronize()
            scores, inds, ys, xs, st_reg, corner_dict = corner_decode(hm[:,1:2,:,:], st, reg, K=MK)
            dets, keep, logi, cr = ctdet_4ps_decode(hm[:,0:1,:,:], wh, ax, cr, corner_dict, reg=reg, K=K, wiz_rev = False)
            corner_output = np.concatenate((np.transpose(xs.cpu()),np.transpose(ys.cpu()),np.array(st_reg.cpu()),np.transpose(scores.cpu())), axis=2)
        return outputs, output, dets, corner_output, logi, cr, keep#, overlayed_map
    
    def post_process(self, dets, meta, corner_st, scale=1):
        dets = dets.detach().cpu().numpy()
        dets = dets.reshape(1, -1, dets.shape[2])
        # return dets is list and what in dets is dict:
        # Dict[classses - [bbox, score]]

        dets = ctdet_4ps_post_process_upper_left(
            dets.copy(), [meta['c']], [meta['s']],
            meta['out_height'], meta['out_width'], 2
        )

        corner_st = ctdet_corner_post_process(
            corner_st.copy(), [meta['c']], [meta['s']],
            meta['out_height'], meta['out_width'], 2
        )

        for j in range(1, 3):
            dets[0][j] = np.array(dets[0][j], dtype=np.float32).reshape(-1, 9)
            dets[0][j][:, :8] /= scale
        return dets[0], corner_st[0]
    
    def merge_outputs(self, detections):
        num_classes, max_per_image = 2, 3000
        results = {}
        for j in range(1, num_classes + 1):
            results[j] = np.concatenate(
                [detection[j] for detection in detections], axis=0
            ).astype(np.float32)
        scores = np.hstack(
            [results[j][:, 8] for j in range(1, num_classes + 1)])
        
        if len(scores) > max_per_image:
            kth = len(scores) - max_per_image
            thresh = np.partition(scores, kth)[kth]
            for j in range(1, num_classes + 1):
                keep_inds = (results[j][:, 8] >= thresh)
                results[j] = results[j][keep_inds]
        return results
            
    
    def run(self, input: np.ndarray, meta=None) -> Dict[str, Any]:
        """
        Args:
            input (`torch.Tensor`): image tensor,
                shape of each tensor is [3, H, W]
        
        Returns:
            dets (`torch.Tensor`): the locations of detected table cells,
                shape of each tensor is [N_cell, 8].
            logi (`torch.Tensor`): the logical coordinates of detected table cells,
                shape of each tensor is [N_cell, 4]
            meta (`Dict`): the meta infor of original image
        
        """
        
        detections = []
        hm = []
        corner_st = []
        image = input
        images, meta = self.preprocess(image, meta=meta)

        images = images.to(self.device)
        torch.cuda.synchronize()

        # outputs = self.detector(images)
        # output = outputs[-1]
        # meta = input['meta']
        # return output

        outputs, output, dets, corner_st_reg, logi, cr, keep = self.process(images, image, return_time= True)
        raw_dets = dets
        torch.cuda.synchronize()

        dets, corner_st_reg = self.post_process(dets, meta, corner_st_reg)
        torch.cuda.synchronize()

        detections.append(dets)
        hm.append(keep)
        results = self.merge_outputs(detections)
        torch.cuda.synchronize()

        # select logical and detections
        slct_logi, slct_dets = filter(results, logi, raw_dets[:, :, :8])
        slct_dets = self._normalized_ps(slct_dets, 256)
        slct_output_dets = results[1][:slct_logi.shape[1], :8]
        print(slct_logi)
        _, slct_logi = self.processor(slct_logi)

        slct_logi = self.process_logi(slct_logi)
        final_results = self.ps_convert_minmax(results) # get approx bbox

        return {
            'dets': slct_output_dets, # select output detections
            'results': final_results, #xyxy
            '4ps': results,
            'logi': slct_logi,
            'corner_st_reg': corner_st_reg,
            'hm': hm
        }

    # def postprocess(self, inputs: Dict[str, Any]) -> Dict[str, Any]:
    #     slct_dets = inputs['dets']
    #     slct_logi = process_logic_output(inputs['logi'])
    #     result = {
    #         'dets': slct_dets,
    #         'boxes': np.array(slct_logi[0].cpu().numpy())
    #     }
    #     return result

        
