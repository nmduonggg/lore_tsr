import copy

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):

        self.val = val
        self.sum += val * n
        if self.val != 0:
            self.count += n
        if self.count > 0:
          self.avg = self.sum / self.count

def grouping_cells(cells, r: float, dist_threshold: float):
    """
    This function grouping cells
    
    Args:
        cells: list of cell(vtx, logi_axis)
            vtx = x1, y1, x2, y2, x3, y3, x4, y4
            logi_axis = start_row, end_row, start_col, end_col
            

    Returns:
        updated_cells: List of cells with vertex coordinates of C updated
    """
    mean_vertex = {}
    updated_cells = copy.deepcopy(cells)

    for i in range(len(cells)):
        ci = cells[i]
        cells_inrow = [ci]
        cells_incols = [ci]
        for j in range(len(cells)):
            cj = cells[j]
            if i != j:
                # Merge rows
                if ci[8] == cj[8] and ci[9] == cj[9]: # same start row and same end row => same row
                    cells_inrow.append(cj)

                # Merge columns
                elif ci[10] == cj[10] and ci[11] == cj[11]: # same start col and same end col => same col
                    cells_incols.append(cj)

        

